<?php
    session_start();

    require 'database.php';


    if (isset($_SESSION['id'])) {
        $statement = $conn->prepare('SELECT id, usuario, password FROM usuarios WHERE id = :id');
        $statement->bindParam(':id', $_SESSION['id']);
        $statement->execute();
        $results = $statement->fetch(PDO::FETCH_ASSOC);
    
        $user = null;
    
        if (count($results) > 0) {
          $user = $results;
        }
      }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Ejercicio 1</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Inicio</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="logout.php">Cerrar Sesion</a>
            </li>
        </ul>
        </div>
    </div>
    </nav>
    <?php

        $sql = $conn->query("SELECT p.producto_id as idproducto, p.nombre as nombreproducto, p.descripcion as descripcion, m.nombre as marcanombre, t.nombre as tiponombre
                             FROM producto p, tipo t, marca m WHERE p.tipo_id = t.tipo_id and p.marca_id = m.marca_id order by 1");
        $result = $sql->fetchAll(PDO::FETCH_OBJ);
        
        echo "<div class='container text-center'";
            echo "<h1> Bienvenido ".$user['usuario']."</h1>";
            echo "<h1 class='text-center'> Listado de Productos </h1>";
            echo "<div class='row d-flex justify-content-center'>";
                echo "<div class='col-md-8'>";
                    echo "<table class='table text-center'>";
                        echo "<tr>";
                            echo "<th> ID </th>";
                            echo "<th> Nombre </th>";
                            echo "<th> Descripcion </th>";
                            echo "<th> Marca </th>";
                            echo "<th> Tipo </th>";
                        echo "</tr>";

                        foreach($result as $resultado) {
                            echo "<tr>";
                                echo "<td>". $resultado->idproducto . "</td>";
                                echo "<td>". $resultado->nombreproducto . "</td>";
                                echo "<td>". $resultado->descripcion . "</td>";
                                echo "<td>". $resultado->marcanombre . "</td>";
                                echo "<td>". $resultado->tiponombre . "</td>";
                                echo "<td><a href='delete.php?id=$resultado->idproducto' class='btn btn-danger'>"."Borrar"."</a></td>"; 
                                echo "<td><a href='update.php?id=$resultado->idproducto' class='btn btn-info'>"."Editar"."</a></td>"; 
                            echo "</tr>";
                        }
                    echo "</table>";
                    echo "<a href='create.php' class='btn btn-primary'>Insertar</a>";
                echo "</div>";
            echo "</div>";
        echo "</div>";
    ?>
    

</body>
</html>