<?php
    require 'database.php';

    try {
        $sql = 'INSERT INTO producto(tipo_id, marca_id, nombre, descripcion) VALUES(?,?,?,?)';
        $statement = $conn->prepare($sql);
        $tipo = $_POST['tipo'];
        $marca = $_POST['marca'];
        $nombre = $_POST['product_name'];
        $descripcion = $_POST['description'];

        $statement->bindParam(1, $tipo);
        $statement->bindParam(2, $marca);
        $statement->bindParam(3, $nombre);
        $statement->bindParam(4, $descripcion);

        $statement->execute();

        if($statement) {
            header("Location: ../../ejercitario5/ejercicio1/index.php");
        } else {
            echo "Ocurrio un error. Intente nuevamente";
        }

        $conn=null;
    } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
?>