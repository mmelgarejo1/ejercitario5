<?php
    include 'database.php';

    $id = $_GET['id'];

    $sql = "SELECT p.nombre as nombreproducto, p.descripcion as descripcion, m.nombre as marcanombre, t.nombre as tiponombre
            FROM producto p, tipo t, marca m WHERE p.producto_id = '$id' AND p.tipo_id = t.tipo_id and p.marca_id = m.marca_id";

    $statement = $conn->prepare($sql);

    $statement->execute();

    $resultado = $statement->fetchAll();

    if($statement) {
        foreach($resultado as $value) {
            $nombreproducto = $value['nombreproducto'];
            $descripcion = $value['descripcion'];
            $nombremarca = $value['marcanombre'];
            $nombretipo = $value['tiponombre'];
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Actualizar Producto</title>
</head>
<body>
<?php
    echo "<div class='container-fluid bg-3 text-center'>";
        echo "<div class='panel panel-primary'>";
            echo "<div class='panel-heading'>Actualizar Producto</div>";
            echo "<br>";
            echo "<form class='form-horizontal' action='updateProducto.php?id=$id' method='post'>";
                echo "<div class='panel-body'>";
                    echo "<div class='row justify-content-center form-group'>";
                        echo "<div class='col-md-3'>";
                            echo "<input class='form-control' type='text' name='product_name' value='".$nombreproducto."' placeholder='Nombre del Producto' autofocus required>";
                        echo "</div>";
                    echo "</div>";
                    echo "<br>";
                    echo "<div class='row justify-content-center form-group'>";
                        echo "<div class='col-md-3'>";
                            echo "<input class='form-control' type='text' name='description' value='".$descripcion."' placeholder='Descripcion' required>";
                        echo "</div>";
                    echo "</div>";
                    echo "<br>";
                        echo "<div class='row justify-content-center form-group'>";
                            echo "<div class='col-md-3'>";
                                echo "<select class='form-select' aria-label='Default select example' name='marca'>";
                                $SelectMarca = 'SELECT nombre FROM marca';
                                $sql = $conn->prepare($SelectMarca);
                                $sql->execute();
                                $resultado=$sql->fetchAll();
                                foreach($resultado as $key => $value) {
                                    if($value['nombre']==$nombremarca) {
                                        echo "<option value='".($key+1)."' selected>".$value['nombre']."</option>";
                                    } else {
                                        echo "<option value='".($key+1)."'>".$value['nombre']."</option>";
                                    }
                                }
                                echo "</select>";
                            echo "</div>";
                        echo "</div>";
                        echo "<br>";
                        echo "<div class='row justify-content-center form-group'>";
                            echo "<div class='col-md-3'>";
                                echo "<select class='form-select' aria-label='Default select example' name='tipo'>";
                                $SelectTipo = 'SELECT nombre FROM tipo';
                                $sql = $conn->prepare($SelectTipo);
                                $sql->execute();
                                $resultado=$sql->fetchAll();
                                foreach($resultado as $key => $value) {
                                    if($value['nombre']==$nombretipo) {
                                        echo "<option value='".($key+1)."' selected>".$value['nombre']."</option>";
                                    } else {
                                        echo "<option value='".($key+1)."'>".$value['nombre']."</option>";
                                    }
                                }
                                echo "</select>";
                                $conn = null;
                            echo "</div>";
                        echo "</div>";
                        echo "<br>";
                    ?>
                    <input type="submit" class="btn btn-success" name="submit" value="Guardar Cambios">
                    <a href="index.php" class="btn btn-primary">Volver al Inicio</a>
                </div>
            </form>
        </div>
    </div>
</body>
</html>