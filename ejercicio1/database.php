<?php
    $server = 'localhost';
    $username = 'postgres';
    $password = '1234';
    $database = 'crud';

    try {
        $conn = new PDO("pgsql:host=$server dbname=$database user=$username password=$password");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOException $e) {
        die('Conexion fallida: ' .$e->getMessage());
    }
?>
