<?php
    session_start();

    require 'database.php';

    if(isset($_POST['submit'])&&!empty($_POST['submit'])) {
        $sql = 'SELECT * FROM usuarios WHERE usuario = :usuario';

        $statement = $conn->prepare($sql);
        $statement->bindParam(':usuario', $_POST['usuario']);
        $statement->execute();
        $resultado = $statement->fetch(PDO::FETCH_ASSOC);
        if($_POST['password'] == $resultado['password'] && count($resultado)>0) {
            $_SESSION['id'] = $resultado['id'];
            header("location: index.php");
        } else {
            echo 'Usuario o contraseña invalidos';
        }
    }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Ingresar</title>
</head>
<body>
    <div class="container-fluid bg-3 text-center">
        <div class="panel panel-primary">
            <div class="panel-heading">Ingresar</div>
            <br>
            <form class="form-horizontal" action="login.php" method="post">
                <div class="panel-body">
                    <div class="row justify-content-center form-group">
                        <div class='col-md-3'>
                            <input class="form-control" type="text" name="usuario" placeholder="Ingresar usuario" autofocus required>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center form-group">
                        <div class='col-md-3'>
                            <input class="form-control" type="password" name="password" placeholder="Ingresar contraseña" required>
                        </div>
                    </div>
                    <br>
                    <input type="submit" name="submit" value="Ingresar" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</body>
</html>
