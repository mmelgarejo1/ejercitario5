<?php
    require '../../ejercitario5/ejercicio1/database.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Crear Producto</title>
</head>
<body>
    <div class="container-fluid bg-3 text-center">
        <div class="panel panel-primary">
            <div class="panel-heading">Registrar Producto</div>
            <br>
            <form class="form-horizontal" action="../../ejercitario5/ejercicio1/insert.php" method="post">
                <div class="panel-body">
                    <div class="row justify-content-center form-group">
                        <div class='col-md-3'>
                            <input class="form-control" type="text" name="product_name" placeholder="Nombre del Producto" autofocus required>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center form-group">
                        <div class='col-md-3'>
                            <input class="form-control" type="text" name="description" placeholder="Descripcion" required>
                        </div>
                    </div>
                    <br>
                    <?php
                        echo "<div class='row justify-content-center form-group'>";
                            echo "<div class='col-md-3'>";
                                echo "<select class='form-select' aria-label='Default select example' name='marca'>";
                                $SelectMarca = 'SELECT nombre FROM marca';
                                $sql = $conn->prepare($SelectMarca);
                                $sql->execute();
                                $resultado=$sql->fetchAll();
                                foreach($resultado as $key => $value) {
                                    echo "<option value='".($key+1)."'>".$value['nombre']."</option>";
                                }
                                echo "</select>";
                            echo "</div>";
                        echo "</div>";
                        echo "<br>";
                        echo "<div class='row justify-content-center form-group'>";
                            echo "<div class='col-md-3'>";
                                echo "<select class='form-select' aria-label='Default select example' name='tipo'>";
                                $SelectTipo = 'SELECT nombre FROM tipo';
                                $sql = $conn->prepare($SelectTipo);
                                $sql->execute();
                                $resultado=$sql->fetchAll();
                                foreach($resultado as $key => $value) {
                                    echo "<option value='".($key+1)."'>".$value['nombre']."</option>";
                                }
                                echo "</select>";
                                $conn = null;
                            echo "</div>";
                        echo "</div>";
                        echo "<br>";
                    ?>
                    <input type="submit" class="btn btn-success" name="submit" value="Guardar Cambios">
                    <a href="../../ejercitario5/ejercicio1/index.php" class="btn btn-primary">Volver al Inicio</a>
                </div>
            </form>
        </div>
    </div>
</body>
</html>