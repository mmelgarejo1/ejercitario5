<?php
    require 'database.php';

    require 'vendor/autoload.php';

    use PhpOffice\PhpSpreadsheet\IOFactory;

    $nombreArchivo = 'insertAlumnos.xlsx';

    $documento = IOFactory::load($nombreArchivo);

    $totalHojas = $documento->getSheetCount();

    $hojaActual = $documento->getSheet(0);
    $numeroFilas = $hojaActual->getHighestDataRow();
    
    for($indiceFila = 2; $indiceFila <= $numeroFilas; $indiceFila++) {

        $nombre = $hojaActual->getCellByColumnAndRow(1, $indiceFila);
        $apellido = $hojaActual->getCellByColumnAndRow(2, $indiceFila);
        $cedula = $hojaActual->getCellByColumnAndRow(3, $indiceFila);
        $matricula = $hojaActual->getCellByColumnAndRow(4, $indiceFila);
        $carrera = $hojaActual->getCellByColumnAndRow(5, $indiceFila);
        $nacionalidad = $hojaActual->getCellByColumnAndRow(6, $indiceFila);

        $sql = "INSERT INTO alumnos(nombre, apellido, cedula, matricula, carrera, nacionalidad) 
                VALUES('$nombre', '$apellido', '$cedula', '$matricula', '$carrera', '$nacionalidad')";

        $statement = $conn->prepare($sql);

        $statement->execute();
    }

    echo "Carga Completa";

?>